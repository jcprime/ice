# Minimal EDC Kit

Ideally to be contained within a [Maxpedition micro pouch]().  Also assuming it would be contained within my rucksack, rather than actually in my pocket or anything.

## Contents

* Tiny AA-powered torch
* Rugged cables (USB/mini-USB, USB/micro-USB, USB/USB-C, etc)
* Solar/hand-crank (ideally both, with potential for mains/computer-based charging) power pack
* Field Notes pocket-size notebook
* Space pen or similar
* Portable screwdriver set
* Portable lock-picking tools that double as other things (because paranoia)
* Arc lighter (because windproof and rechargeable via mains/USB/etc)
