# Power and energy

Use rechargeable AA batteries wherever possible; get equipment that use AA if they run on batteries at all.

For devices which have non-standard (i.e. anything other than mains AC or original USB) connectors, stick to those with a single kind of connector wherever possible, to avoid having to worry about amassing bundles of every kind of cable imaginable.  Although it would still be a good idea to have a couple of cables for each of the more standard kinds of connectors, such as mini-USB, micro-USB, even USB-C - but if you can get devices that only use one of those types, and avoid those with specialised or proprietary connectors wherever possible, that'd be good.

If you can get things that perform more than one function, great; even more so if you can find stuff that can also be charged via USB, solar, and hand-crank.

## Solar

Goal Zero is a good brand.