# Prepping Lists

#### EDC/Pockets

##### Keyring attachments

* Encrypted USB drive
* Bobby pins
* Paracord ("550 cord")
* 9cm-long [NiteCore CREE XP-G R5 MT1C Multitask LED **Flashlight**](http://www.amazon.com/dp/B00937YE6C/?tag=h1111-20)
* [Fenix LD10 **Flashlight**](http://www.amazon.com/gp/product/B004DVIJSW?ie=UTF8&camp=1789&creativeASIN=B004DVIJSW&linkCode=xm2&tag=2551edc-20), as recommended by Greywolf Survival
* [Zebralight SC52w](https://www.amazon.com/Zebralight-SC52w-L2-Flashlight-Neutral/dp/B00K5B4A8W/ref=as_sl_pc_ss_til?tag=2551edc-20&linkCode=w01&linkId=YBMS2YJLHTZLBEXQ&creativeASIN=B00K5B4A8W), as recommended by Greywolf Survival (not actually a keyring-sized light though)

##### Stationery
* [Gerber Tactical **Pen**](http://www.amazon.com/dp/B00B0BD3W0/?tag=h1111-20)

##### Watch
* [Marathon GSAR US Government Issue **Watch**](http://www.amazon.com/gp/product/B004U3NZLY?ie=UTF8&camp=1789&creativeASIN=B004U3NZLY&linkCode=xm2&tag=2551edc-20), as recommended by Greywolf Survival

##### Firestarters
* [ITS Tactical Brass **Firestarter**](https://store.itstactical.com/its-brass-fire-starter.html)
* [ITS Swedish More **Fireknife**](https://store.itstactical.com/its-engraved-swedish-mora-fireknife.html)
* [Aurora 440C **Firestarter**](http://www.amazon.com/gp/product/B005D36ZPS?ie=UTF8&camp=1789&creativeASIN=B005D36ZPS&linkCode=xm2&tag=2551edc-20), as recommended by Greywolf Survival

##### Water purifiers

##### Multitool (for repairs and things)
* Aaaargh
* [CRKT Pryma](http://everydaycarry.com/posts/26746/crkt-pryma-multi-tool) (tiny hand-held hand-safe blade with various other little utilities)

##### Lock-picking kit/skeleton key

##### Reference materials
* Homesteading PDFs
* Survival PDFs
* Field manuals

#### Bug Out Bag

[Tactical Tailor Operator Removable Pack](http://www.amazon.com/Tactical-Tailor-Operator-Removable-Pack/dp/B00EOXO9Z6?tag=pd999-20)

* Emergency phone
* Universal charger
* Universal adapter/converter
* Fooood (non-perishable)
* Water
* Water purifiers again
* Important information
* Money in cash form
* Space blanket
* Maps, maps, maaaaps
* Spare clothes
* First aid kit
* Longer-term medical supplies (glasses, contacts, hearing aids, syringes, etc)
* Two-way radios
* All the spare keys
* Manual can opener

* Whistle
* Matches
* Rain gear
* Towels


##### Knives
* [Gerber Fine and Serrated **Knife**](http://www.amazon.com/dp/B000KSCEH4/?tag=pf1111-20)
* Contour-y handle [Kershaw 1870 Knockout SpeedSafe **Folding Knife**](http://www.amazon.com/dp/B0059BLJP0/?tag=h1111-20)
* [Boker Plus Subcom **Knife**](http://www.amazon.com/gp/product/B0016CT8HM?ie=UTF8&camp=1789&creativeASIN=B0016CT8HM&linkCode=xm2&tag=2551edc-20), as recommended by Greywolf Survival

##### Miscellaneous
* [Olive **Shemagh**](http://www.amazon.com/dp/B00AHZC06U/?tag=h1111-20), just because...

##### Left open in tabs that I needed to close!
* [Cree 7W Flashlight](http://graywolfsurvival.com/2707/prepper-gear-review-7w-300lm-cree-edc-flashlight/)
* [Buck Hartsook Fixed Blade Neck Knife](http://www.amazon.com/gp/product/B000MX6G8U?ie=UTF8&camp=1789&creativeASIN=B000MX6G8U&linkCode=xm2&tag=2551edc-20)
* [Where There Is No Dentist (book)](http://www.amazon.com/Where-There-Is-No-Dentist/dp/0942364058/ref=pd_sim_14_3?ie=UTF8&dpID=61emvBmNwYL&dpSrc=sims&preST=_AC_UL160_SR108%2C160_&refRID=13XGHPMY8M47AZXZK2HP)
* [Silva Guide 426 Compass](http://www.amazon.com/dp/B000EQ81SS/?tag=pf1111-20)
* [Mr Pry](http://cogentindustries.com/products/mrpry)
* [Boker Plus Subcom Titan Knife](http://www.amazon.com/dp/B0037F34R6/?tag=h1111-20)
* [Sanrenmu 763 LB-763 Pocket Knife](http://www.amazon.com/dp/B01DD7P24A/?tag=h1111-20)
* [Pacsafe Venturesafe 325 GII Anti-Theft Cross Body Pack](http://www.amazon.com/dp/B00DOBZ2L4/?tag=pd999-20)
* [Spyderco Dragonfly 2 Lightweight Knife](http://www.amazon.com/dp/B00DJTV3M8/?tag=h1111-20)