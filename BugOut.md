# Bugging out (or in)

## Power

* Solar generator, such as:
    - [Goal Zero Yeti 150](https://www.amazon.co.uk/Goal-Zero-Yeti-Universal-Power/dp/B00EVV24LM/)
    - [Goal Zero Yeti 400](https://www.amazon.co.uk/dp/B00JJDPTYQ)
    - [Goal Zero Yet 1250 with roller-cart thingy, so as not to have to lift it](https://www.amazon.co.uk/Goal-Zero-Yeti-Solar-Generator/dp/B00P10STQ6/)