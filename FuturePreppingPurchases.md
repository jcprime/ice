# List of Impending Purchases

Mini-purchases:

* Pocket knife sharpener: [Comparisons](http://www.gearassistant.com/best-pocket-knife-sharpener/)
* Watch strap compass (not the best/most reliable, but better than nothing)
* Mini power pack, such as:
    - [Goal Zero Flip 10](https://www.amazon.co.uk/Goal-Zero-Flip-10-Recharger/dp/B0168U741S/)
    - [Goal Zero Flip 20](https://www.amazon.co.uk/Goal-Zero-Flip-20-Recharger/dp/B00U12PNNG/)
    - [Goal Zero Flip 30]()

Investment purchases:

* Solar panel charger (**note** that these do not *hold* charge themselves; they directly power either the charge-less devices themselves or a separate power bank), such as:
    - [Goal Zero Nomad 7](https://www.amazon.co.uk/Goal-Zero-Nomad-Solar-Panel/dp/B00D2SPZAM/); ~£80; note that it does not charge tablets via USB, but will charge mobile phones, GPS, etc., and will also charge 12V devices
    - [Goal Zero Nomad 13](https://www.amazon.co.uk/Goal-Zero-Nomad-12003-Solar/dp/B00A2F2U1K/); ~£110
    - [Goal Zero Nomad 20](https://www.amazon.co.uk/Goal-Zero-Nomad-Solar-Panel/dp/B00GU64KV8/); ~£160
* Solar battery-recharger, such as:
    - [Goal Zero Guide 10 Plus](https://www.amazon.co.uk/Goal-Zero-Guide-Plus-Power/dp/B00D2SQ6W8/); ~£40; charges rechargeable AA and AAA batteries, and can also charge USB devices; can be charged via solar or USB; also can be used as a torch
* Solar panel power bank, such as:
    - [Goal Zero Venture 30](https://www.amazon.co.uk/GOAL-ZERO-22008-Venture-30-Black/dp/B00TY2HXFU/); ~£90; can be charged via solar or USB
    - [Goal Zero Venture 70](https://www.amazon.co.uk/Goal-Zero-Venture-Recharger-Lightning/dp/B01M1RSBA8/); ~£150; also has torch built in
* Multi-purpose, multi-charge-able torch, such as:
    - [Goal ZeroTorch 250 Power Hub](https://www.amazon.co.uk/Goal-Zero-Torch-250-Power/dp/B00JGR3QT0/); ~£70; can be used as flashlight, floodlight, or red emergency light; can charge USB devices with stored energy; can be charged via solar or hand-crank
* Portable power bank (not solar, just portable), such as:
    - [Goal Zero Sherpa 50](https://www.amazon.co.uk/Goal-Zero-GZ-62206-Sherpa-Power/dp/B00QVOTTYC/); ~£175; **NEED MORE INFORMATION**
    - [Goal Zero Sherpa 100](https://www.amazon.co.uk/Goal-Zero-Sherpa-Grey-Charger/dp/B00P7RITEU/); ~£320; can charge USB, 12V, and mains AC; can be charged by USB, 12V, and (separate) solar panels
* Hand-crank generator (i.e. hand-crank-able power bank), such as:
    - [K-Tor Pocket Socket USB 1A Charger](https://www.k-tor.com/usb-hand-crank-generator/); distinct from K-Tor Pocket Socket (original), which has a US-mains socket rather than a USB port
    - [K-Tor Power Box](https://www.k-tor.com/Power-Box); pedal-powered charger; only has US-mains socket, so will need an adapter; 4lb 11oz, and 12 x 5.5 x 3.5 inches

Extortionate purchases:

* Solar generator, such as:
    - [Goal Zero Yeti 150](https://www.amazon.co.uk/Goal-Zero-Yeti-Universal-Power/dp/B00EVV24LM/); ~£180; 12lb, or 5.4kg, and 28 x 40 x 37cm; can power smartphones, cameras, tablets, and laptops; rechargeable by solar, mains AC, or 12V
    - [Goal Zero Yeti 400](https://www.amazon.co.uk/dp/B00JJDPTYQ); ~£410; 29lb, or 13.2kg, and 26 x 20 x 20cm; can power everything the 150 can, plus TVs; rechargeable by same as the 150
    - [Goal Zero Yet 1250](https://www.amazon.co.uk/dp/B00W6PWQ5E); ~£1400; 47kg, and 41 x 30 x 37cm; can power everything the 400 can, plus fridges; rechargeable by same as 400; can get a [roller-cart thingy so as not to have to lift it](https://www.amazon.co.uk/Goal-Zero-Yeti-Solar-Generator/dp/B00P10STQ6/)