# Pocket EDC Kit

Ideally to be very easily pocket-sized, for actual pocket-carrying.  Only likely to be needed or used in the case where I don't have a need to bring my whole rucksack (with minimal EDC kit inside) with me.  Probably contained within an Altoid-sized tin or some such container.

