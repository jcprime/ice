# Skills

## Communication

### Ham radio

* Callsign:


## Fire

### Methods for lighting fires

* Solar ignition
    - Parabolic reflectors
    - Fresnel lenses (are flat and credit-card-sized ones can work well)
    - High-magnification lenses
* Rod/flint striking
    - Ferrocerium rods
    - Magnesium rods
* Liquid lighter
    - Ubiquitous
* Arc lighter
    - Windproof
* Matches
    - Ideally get waterproof/windproof/whatever-proof kind in a sealed capsule
* Bow drill (and similar)

### Firewood types

From lightest to heaviest/thickest:

* Tinder (the thing you want to set alight)
* Kindling (the thing you use the lit tinder to set alight)
* Fuel wood (the thing you actually want to power the proper fire with)

### Effects of different wood on fire quality

* Pine is a big burner (sap is excellent for making tinder more flammable), but burns dirty


## Medical

### Natural alternatives/derivatives



## Navigation

### Finding north

* Making own compass
* Using moss (which prefers shade) and knowledge of your hemisphere
* Following sun's movement, if visible, across East-West line


## Shelter



## Signalling



## Water

### Methods for purifying water

* Boiling
* UV purification
* Distillation

### Distillation process


