# Links

## General information blogs/websites

* [I Fucking Love Survival (Tumblr)](https://ifuckinglovesurvival.tumblr.com/) is auto-blogged using IFTTT
* [Off the Grid News](http://www.offthegridnews.com/)
* [Preparedness & Survival (Tumblr)](http://preparednessandsurvival.tumblr.com/)
* [Survival Life](https://survivallife.com/)
* [Graywolf Survival](https://graywolfsurvival.com/)

## Fitness

[10 Ways Your Grandparents Stayed in Shape Without a Gym](http://www.offthegridnews.com/alternative-health/10-ways-your-grandparents-stayed-in-shape-without-a-gym/) essentially just advocates doing everything manually, whether you *need* to or not, basically.

## Food

[Making Pemmican: The Ultimate Survival Food](https://survivallife.com/making-pemmican-survival-food/) introduces us to a foodstuff I'd never heard of!

[Survival Foods 101: 43 Survival Foods That Taste Good](https://survivallife.com/survival-food-that-taste-good/) reminds us of some things we may not have previously thought of as survival food.

## Gardening

[Here's the skinny on saving seeds](http://thesurvivalmom.com/heres-the-skinny-on-saving-seeds/) by Survival Mom links to [FEDCO's Saving Seeds for Beginners](https://www.fedcoseeds.com/seeds/seed_saving.htm) for information about optimal seed-related things (and for buying quality seeds for storage as well), and recommends [Steve Solomon's *Gardening when it counts: growing food in hard times*](http://www.amazon.com/gp/product/086571553X) for subsistence gardening information.

[Heirloom Seeds: Are They Part Of Your Preparedness Plan?](https://survivallife.com/heirloom-seeds/)

## Navigation

[*Lost in the Woods: 5 Tricks for Finding Your Way Without a Compass*](http://www.offthegridnews.com/extreme-survival/lost-in-the-woods-5-tricks-for-finding-your-way-without-a-compass/) suggests several ways to navigate under different conditions.  One big takeaway is that you should **always have access to a needle and some wool or silk (for magnetising the needle), and something to allow the needle to float freely** (like a leaf on a puddle in a wind-shielded area).  Additionally, remember that north is straight ahead if east is on your right (which I always somehow manage to forget).  **Moss favours shade**, so if you're in the Northern hemisphere, a solitary tree with good sun exposure will have moss growing on the north side.

## Security

[14 DIY Badass Weapons That Can Save Your Life When SHTF](https://survivallife.com/7-badass-weapons-can-make-home/)

[11 Types of Guns That'll Keep You Alive on Doomsday](https://outdoorwarrior.com/types-of-guns-doomsday/)

[How to Make a DIY Death Ray](https://survivallife.com/make-diy-death-ray/)

## Skills

[17 Amazing Ways Duct Tape Can Save Your Life](http://www.offthegridnews.com/extreme-survival/17-amazing-ways-duct-tape-can-save-your-life-hopefully-youll-never-have-to-try-no-10/)
