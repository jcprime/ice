# Communication

## Ham radio

### Hardware

Some suggestions from [Graywolf](https://graywolfsurvival.com/2716/ham-radio-best-shtfdisaster-communication/) (first are those in his current use):

* Baofeng UV-5R for affordable handheld station (see [Amazon](https://www.amazon.co.uk/BaoFeng-UV-5R-136-174-400-480-Dual-Band/dp/B007HH6RR4/ref=sr_1_3?ie=UTF8&qid=1515767018&sr=8-3&keywords=Baofeng+UV-5R); ~£30)
* Yaesu VX-6R for handheld station (see [Amazon](https://www.amazon.co.uk/Tri-Band-Yaesu-VX-6R-Submersible-Transceiver/dp/B004ESEW6C/ref=sr_1_1?ie=UTF8&qid=1515766164&sr=8-1&keywords=Yaesu+VX-6R); ~£200)
* Yaesu FT-857D for portable station (see [Amazon](https://www.amazon.co.uk/FT857D-Amateur-10-100-Mobile-Transceiver/dp/B003YCJLDS/ref=sr_1_2?ie=UTF8&qid=1515766554&sr=8-2&keywords=Yaesu+FT-857D); ~£750)

Honourable mentions:

* Uniden BC125AT for handheld station (see [Amazon](https://www.amazon.co.uk/Uniden-BC125AT-two-way-radio-radios/dp/B00772MR0K/ref=sr_1_1?ie=UTF8&qid=1515765885&sr=8-1&keywords=Uniden+BC125AT); ~£190)
* Yaesu VX-8DR for handheld station (no longer available, apparently, either in UK or US)
* Yaesu VX-8GR for handheld station (no longer available, apparently, either in UK or US)
* Diamond (Original) SRH77CA **as an upgrade antenna** (can't find on UK Amazon, but see [US Amazon](https://www.amazon.com/gp/product/B00K1JJWFO/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00K1JJWFO&linkCode=as2&tag=shtfcomms2716-20&linkId=TMFABBMSUZHBLBAA); ~$30)

### Channels

#### United Kingdom



#### United States

**Note:** The following details came from [Graywolf](https://graywolfsurvival.com/2716/ham-radio-best-shtfdisaster-communication/) again.  He also links to [this list of frequencies](http://www.textfiles.com/hamradio/FREQUENCIES/).

Frequency | Usage
----------|----------------------------------------------------------------------------------------
34.90     | Used nationwide by the National Guard during emergencies.
39.46     | Used for inter-department emergency communications by local and state police forces.
47.42     | Used across the United States by the Red Cross for relief operations.
52.525    | Calling frequency used by ham radio operators in FM on their six-meter band.
121.50    | International aeronautical emergency frequency.
138.225   | Disaster relief operations channel used by the Federal Emergency Management Agency; it is active during earthquakes, hurricanes, floods, and other catastrophic events.
146.52    | Used by ham radio operators for non-repeater communications on the two-meter band; it is very busy in many parts of the country.
151.625   | Used by "itinerant" businesses, or those that travel about the country. Circuses, exhibitions, trade shows, and sports teams are some of the users you can hear. Other widely used itinerant channels are 154.57 and 154.60.
154.28    | Used for inter-department emergency communications by local fire departments; 154.265 and 154.295 also used.
155.160   | Used for inter-department emergency communications by local and state agencies during search and rescue operations.
155.475   | Used for inter-department emergency communications by local and state police forces.
156.75    | Used internationally for broadcasts of maritime weather alerts.
156.80    | International maritime distress, calling, and safety channel. All ships must monitor this frequency while at sea. It is also heavily used on rivers, lakes, etc.
162.40    | NOAA weather broadcasts and bulletins.
162.425   | NOAA weather broadcasts and bulletins.
162.45    | NOAA weather broadcasts and bulletins.
162.475   | NOAA weather broadcasts and bulletins.
162.50    | NOAA weather broadcasts and bulletins.
162.525   | NOAA weather broadcasts and bulletins.
162.55    | NOAA weather broadcasts and bulletins.
163.275   | NOAA weather broadcasts and bulletins.
163.4875  | Used nationwide by the National Guard during emergencies.
163.5125  | The national disaster preparedness frequency used jointly by the armed forces.
164.50    | National communications channel for the Department of Housing and Urban Development.
168.55    | National channel used by civilian agencies of the federal government for communications during emergencies and disasters.
243.00    | Used during military aviation emergencies.
259.70    | Used by the Space Shuttle during re-entry and landing.
296.80    | Used by the Space Shuttle during re-entry and landing.
311.00    | Flight channel used by the U.S. Air Force.
317.70    | Used by U.S. Coast Guard aviation.
317.80    | Used by U.S. Coast Guard aviation.
319.40    | Used by the U.S. Air Force.
340.20    | Used by U.S. Navy aviators.
409.20    | National communications channel for the Interstate Commerce Commission.
409.625   | National communications channel for the Department of State.
462.675   | Used for emergency communications and traveler assistance in the General Mobile Radio Service.

## Emergency shortwave radio

Ideally should be solar and/or hand-crank.  Enables listening in to weather bulletins etc, but does not have the range of a ham radio, nor does it allow you to transmit.

[Graywolf recommends](https://graywolfsurvival.com/2716/ham-radio-best-shtfdisaster-communication/):

* Safe-T-Proof Solar + Hand-Crank Emergency Radio (and torch, beacon, phone charger)

## Smartphone-related radio apps

Another article from [Graywolf](https://graywolfsurvival.com/3541/send-secure-messages-even-without-cell-service-gotenna/) highlights the [goTenna](https://www.gotenna.com/) app/service.  It apparently connects to your phone via Bluetooth and then transmits an encrypted text by radio signal.  It does need the people you want to contact to also have the goTenna app and antenna/mesh/suchlike though.
